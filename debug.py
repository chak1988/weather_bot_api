def get_value(value:int)->str:
    if value > 0:
        return 'positive'
    elif value < 0:
        return 'negative'

if __name__=="__main__":
    value = int(input('Input your number : '))
    print(get_value(value).casefold())