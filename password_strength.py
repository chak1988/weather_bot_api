import string

def password_strength(value: str) -> str:
    digits = string.digits
    lowers = string.ascii_lowercase
    uppers = string.ascii_uppercase

    if len(value) < 8:
        return 'Too Weak'

    counter = 0
    for symbols in (digits,lowers,uppers):
        if any(char in symbols for char in value):
            counter += 1
            continue
    if counter == 3:
        return 'Very Good'
    return 'Weak' if counter == 1 else 'Good'
    # if all(char in digits for char in value) or all(char in lowers for char in value) or all(char in uppers
    #                                                                                          for char in value):
    #     return 'Weak'
    # if any(char in digits for char in value) and any(char in lowers for char in value) and any(char in uppers
    #                                                                                            for char in value):
    #     return 'Very Good'
    # if any(char in digits for char in value) and any(char in lowers for char in value) or (
    #     any(char in digits for char in value) and any(char in uppers for char in value) ) or (
    #     any(char in lowers for char in value) and any(char in uppers for char in value)):
    #     return 'Good'


if __name__ == "__main__":
    assert password_strength('123') == 'Too Weak'
    assert password_strength('asddfs') == 'Too Weak'
    assert password_strength('ASDDFDF') == 'Too Weak'
    assert password_strength('12345678') == 'Weak'
    assert password_strength('asdfghjk') == 'Weak'
    assert password_strength('ASDFGHJY') == 'Weak'
    assert password_strength('1234567a') == 'Good'
    assert password_strength('asdfghj1') == 'Good'
    assert password_strength('ASDFGHJ2') == 'Good'
    assert password_strength('ASD123asd') == 'Very Good'
    assert password_strength('Aa123456') == 'Very Good'


