from unittest import TestCase, main
from my_weather_bot import sum_


class WeatherTest(TestCase):

    def test_sum(self):
        self.assertEqual(sum_(4,8),12)

    def test_sum2(self):
        self.assertEquals(sum_(20,20),40)

    def test_sum3(self):
        self.assertEquals(sum_(12,13),25)


if __name__ == '__main__':
    main()